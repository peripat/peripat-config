"use strict";
exports.__esModule = true;
exports.inboxItemActionIsBulk = exports.inboxItemActions = void 0;
exports.inboxItemActions = {
    CREATE_NOTIFICATION: 'CREATE_NOTIFICATION',
    MARK_AS_CHECKED: 'MARK_AS_CHECKED',
    MARK_AS_NOT_CHECKED: 'MARK_AS_NOT_CHECKED',
    MARK_AS_VIEWED: 'MARK_AS_VIEWED',
    MARK_AS_NOT_VIEWED: 'MARK_AS_NOT_VIEWED',
    CLEAR_INBOX_ITEM_NOTIFICATIONS: 'CLEAR_INBOX_ITEM_NOTIFICATIONS'
};
exports.inboxItemActionIsBulk = {
    CREATE_NOTIFICATION: false,
    MARK_AS_CHECKED: true,
    MARK_AS_NOT_CHECKED: true,
    MARK_AS_VIEWED: true,
    MARK_AS_NOT_VIEWED: true,
    CLEAR_INBOX_ITEM_NOTIFICATIONS: true
};
