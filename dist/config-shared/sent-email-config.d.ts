export declare const sentEmailStatuses: {
    AWAITING_DELIVERY: string;
    DELIVERED: string;
    UNLINKED: string;
};
