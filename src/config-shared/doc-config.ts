export const docCaptureStatuses = {
  NEW: 'NEW',
  CAPTURED: 'CAPTURED',
  CLEANED: 'CLEANED',
  ORG_APPLIED: 'ORG_APPLIED',
  RESULT: 'RESULT'
};

export const docCaptureResults = {
  RECORDED: 'RECORDED',
  DUPLICATE: 'DUPLICATE',
  SKIPPED: 'SKIPPED',
  NOTIFICATION: 'NOTIFICATION',
  DELETED: 'DELETED'
};

// the results which means that the doc capture is completed. Basically everything except notification
export const completedDocCaptureResults = [
  docCaptureResults.RECORDED,
  docCaptureResults.DUPLICATE,
  docCaptureResults.DELETED,
  docCaptureResults.SKIPPED
];

export const accountingExportResults = {
  AWAITING_EXPORT: 'AWAITING_EXPORT',
  TEXT_DONE: 'TEXT_DONE',
  ATTACHMENT_DONE: 'ATTACHMENT_DONE',
  EXPORTED: 'EXPORTED',
  ERROR: 'ERROR',
  RETRY: 'RETRY',
  EXPORT_SKIPPED: 'EXPORT_SKIPPED'
};

// do not run the export for the following export results
export const notRunAccountingExportResults = [
  accountingExportResults.EXPORTED,
  accountingExportResults.ERROR,
  accountingExportResults.EXPORT_SKIPPED
];

export const docStatuses = {
  CAPTURE_INCOMPLETE: 'CAPTURE_INCOMPLETE',
  APPROVAL_INCOMPLETE: 'APPROVAL_INCOMPLETE',
  EXPORT_INCOMPLETE: 'EXPORT_INCOMPLETE',
  PROCESSING_COMPLETE: 'PROCESSING_COMPLETE',
  UNKNOWN: 'UNKNOWN'
};

export const docApprovalStatuses = {
  AWAITING_APPROVAL: 'AWAITING_APPROVAL', // no approvals have been completed, but some are there. This can be edited at this stage
  PART_APPROVED: 'PART_APPROVED', // some have been completed but none are rejected
  REJECTED: 'REJECTED', // an approval has been rejected and either the doc remains so is blocking it from being resent by the contact or it was marked as deleted
  APPROVED: 'APPROVED' // completely approved
};

export const completedDocApprovalStatuses = [
  docApprovalStatuses.REJECTED,
  docApprovalStatuses.APPROVED
];

// reconciled status options for statement lines
export const reconciledStatuses = {
  PROCESSED: 'PROCESSED',
  MISSING: 'MISSING',
  ['N/A']: 'N/A'
};

// these are the statuses for tracked missing invoices.
export const missingInvoiceStatuses = {
  NEW: 'NEW',
  REPORT_SENT: 'REPORT_SENT',
  REMINDER_1_SENT: 'REMINDER_1_SENT',
  REMINDER_2_SENT: 'REMINDER_2_SENT',
  USER_NOTIFICATION_CREATED: 'USER_NOTIFICATION_CREATED',
  TRACKING_EXPIRED: 'TRACKING_EXPIRED', // Was tracking for too long so time expired
  STOPPED_TRACKING: 'STOPPED_TRACKING', // either stopped by the user because a new statement was sent
  INVOICE_RECEIVED: 'INVOICE_RECEIVED'
};

// the statuses where we are still waiting for the invoice to come in. The others should have isTracked=false anyway
export const missingInvoiceStatusesOutstanding = [
  missingInvoiceStatuses.NEW,
  missingInvoiceStatuses.REPORT_SENT,
  missingInvoiceStatuses.REMINDER_1_SENT,
  missingInvoiceStatuses.REMINDER_2_SENT,
  missingInvoiceStatuses.USER_NOTIFICATION_CREATED
];

export const deliveryConfirmationTypes = {
  INVOICE: 'INVOICE',
  DELIVERY_NOTE: 'DELIVERY_NOTE',
  PURCHASE_ORDER: 'PURCHASE_ORDER'
};

export const docMatchStatuses = {
  AWAITING_MATCH: 'AWAITING_MATCH',
  PART_MATCH: 'PART_MATCH',
  MATCHED: 'MATCHED',
  OVER_MATCHED: 'OVER_MATCHED'
};