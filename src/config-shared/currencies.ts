// https://en.wikipedia.org/wiki/ISO_4217

export const currencies = {
  AUD: 'AUD',
  CAD: 'CAD',
  CHF: 'CHF',
  EUR: 'EUR',
  GBP: 'GBP',
  JPY: 'JPY',
  NOK: 'NOK',
  NZD: 'NZD',
  SEK: 'SEK',
  USD: 'USD'
}

export const currencyProperties = [
  { currency: currencies.AUD, currencySymbol: '$', currencyName: 'Australian Dollar' },
  { currency: currencies.CAD, currencySymbol: '$', currencyName: 'Canadian Dollar' },
  { currency: currencies.CHF, currencySymbol: 'Fr', currencyName: 'Swiss Franc' },
  { currency: currencies.EUR, currencySymbol: '€', currencyName: 'Euro' },
  { currency: currencies.GBP, currencySymbol: '£', currencyName: 'British Pound Sterling' },
  { currency: currencies.JPY, currencySymbol: '¥', currencyName: 'Japanese Yen' },
  { currency: currencies.NOK, currencySymbol: 'kr', currencyName: 'Norwegian Krone' },
  { currency: currencies.NZD, currencySymbol: '$', currencyName: 'New Zealand Dollar' },
  { currency: currencies.SEK, currencySymbol: 'kr', currencyName: 'Swedish Krona' },
  { currency: currencies.USD, currencySymbol: '$', currencyName: 'United States Dollar' }
];
