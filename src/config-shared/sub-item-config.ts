export const subItemTypes = {
  EMAIL: 'EMAIL',
  PDF: 'PDF',
  IMAGE: 'IMAGE',
  SPREADSHEET: 'SPREADSHEET',
  DOCUMENT: 'DOCUMENT',
  TEXT: 'TEXT',
  HTML: 'HTML',
  JSON: 'JSON',
  XML: 'XML',
  CSV: 'CSV',
  COMPRESSED: 'COMPRESSED',
  EML: 'EML', // when an email is an attachement (I think it is an outlook thing). We already extract the attachments.
  UNKNOWN_FILE: 'UNKNOWN_FILE'
};

export const subItemResults = {
  PROCESSED: 'PROCESSED',
  INCOMPLETE: 'INCOMPLETE',
  NOTIFICATION: 'NOTIFICATION'
};

// includes "."
export const extensionsForSubItemTypes = {
  PDF: ['.pdf'],
  IMAGE: ['.png', '.gif', '.jpg', '.jpeg', '.bmp', '.tif'],
  SPREADSHEET: ['.xls', '.xlsx'],
  DOCUMENT: ['.docx'],
  TEXT: ['.txt'],
  HTML: ['.html'],
  JSON: ['.json'],
  XML: ['.xml'],
  CSV: ['.csv'],
  COMPRESSED: ['.zip', '.tar', '.gz'],
  EML: ['.eml']
};


// extensions that can be emailed but aren't a sub item type
const otherSendableExtensions = ['.doc'];

// the extension that we allow to be sent via email. We don't send compressed as we have unzipped them, same with eml
export const sendAttachmentExtensions = [
  ...extensionsForSubItemTypes[subItemTypes.PDF],
  ...extensionsForSubItemTypes[subItemTypes.IMAGE],
  ...extensionsForSubItemTypes[subItemTypes.SPREADSHEET],
  ...extensionsForSubItemTypes[subItemTypes.DOCUMENT],
  ...extensionsForSubItemTypes[subItemTypes.TEXT],
  ...extensionsForSubItemTypes[subItemTypes.HTML],
  ...extensionsForSubItemTypes[subItemTypes.JSON],
  ...extensionsForSubItemTypes[subItemTypes.XML],
  ...extensionsForSubItemTypes[subItemTypes.CSV],
  ...otherSendableExtensions
];