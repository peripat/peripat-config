export const parserEngines = {
  EMAIL: 'EMAIL',
  PDF: 'PDF',
  IMAGE: 'IMAGE',
  SPREADSHEET: 'SPREADSHEET',
  DOCUMENT: 'DOCUMENT',
  TEXT: 'TEXT',
  HTML: 'HTML',
  JSON: 'JSON',
  XML: 'XML',
  CSV: 'CSV',
}

export const engineVersions = {
  v1: 'v1',
  'R270_v1': 'R270_v1',
  RAW: 'RAW'
}

export const parserEngineVersions = {
  EMAIL: [engineVersions.v1],
  PDF: [engineVersions.v1, engineVersions.R270_v1],
  IMAGE: [engineVersions.v1],
  SPREADSHEET: [engineVersions.v1],
  DOCUMENT: [engineVersions.v1],
  TEXT: [engineVersions.v1, engineVersions.RAW],
  HTML: [engineVersions.v1, engineVersions.RAW],
  JSON: [engineVersions.v1, engineVersions.RAW],
  XML: [engineVersions.v1, engineVersions.RAW],
  CSV: [engineVersions.v1, engineVersions.RAW]
};

export const defaultParserEngineVersions = {
  EMAIL: [
    {
      parserEngine: parserEngines.EMAIL,
      engineVersion: engineVersions.v1
    }
  ],
  PDF: [
    {
      parserEngine: parserEngines.PDF,
      engineVersion: engineVersions.v1
    }
  ],
  IMAGE: [
    {
      parserEngine: parserEngines.IMAGE,
      engineVersion: engineVersions.v1
    }
  ],
  SPREADSHEET: [
    {
      parserEngine: parserEngines.SPREADSHEET,
      engineVersion: engineVersions.v1
    }
  ],
  DOCUMENT: [
    {
      parserEngine: parserEngines.DOCUMENT,
      engineVersion: engineVersions.v1
    }
  ],
  TEXT: [
    {
      parserEngine: parserEngines.TEXT,
      engineVersion: engineVersions.v1
    }
  ],
  HTML: [
    {
      parserEngine: parserEngines.HTML,
      engineVersion: engineVersions.v1
    }
  ],
  JSON: [
    {
      parserEngine: parserEngines.JSON,
      engineVersion: engineVersions.RAW
    }
  ],
  XML: [
    {
      parserEngine: parserEngines.XML,
      engineVersion: engineVersions.RAW
    }
  ],
  CSV: [
    {
      parserEngine: parserEngines.CSV,
      engineVersion: engineVersions.RAW
    }
  ],
  COMPRESSED: [],
  EML: [],
  UNKNOWN_FILE: []
};
