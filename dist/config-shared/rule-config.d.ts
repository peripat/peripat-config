export declare const ruleExecutionPoints: {
    PRE_CAPTURE_INBOX_ITEM: string;
    PRE_CAPTURE_SUB_ITEM: string;
    CAPTURE_DOC: string;
    CLEANUP_DOC: string;
    APPLY_TO_DOC: string;
    DOC_RESULT: string;
    NO_MATCH_ON_SUB_ITEM: string;
    SENDER_UNKNOWN: string;
};
export declare const ruleActionTypes: {
    SET_VALUE: string;
    SET_UPPER_CASE: string;
    SET_LOWER_CASE: string;
    REPLACE_STR: string;
    APPEND_STR: string;
    CREATE_NOTIFICATION: string;
    THROW_ERROR: string;
    CAPTURE_FIELD: string;
    ADD_TRACKING: string;
};
