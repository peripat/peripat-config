"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
exports.__esModule = true;
exports.sendAttachmentExtensions = exports.extensionsForSubItemTypes = exports.subItemResults = exports.subItemTypes = void 0;
exports.subItemTypes = {
    EMAIL: 'EMAIL',
    PDF: 'PDF',
    IMAGE: 'IMAGE',
    SPREADSHEET: 'SPREADSHEET',
    DOCUMENT: 'DOCUMENT',
    TEXT: 'TEXT',
    HTML: 'HTML',
    JSON: 'JSON',
    XML: 'XML',
    CSV: 'CSV',
    COMPRESSED: 'COMPRESSED',
    EML: 'EML',
    UNKNOWN_FILE: 'UNKNOWN_FILE'
};
exports.subItemResults = {
    PROCESSED: 'PROCESSED',
    INCOMPLETE: 'INCOMPLETE',
    NOTIFICATION: 'NOTIFICATION'
};
// includes "."
exports.extensionsForSubItemTypes = {
    PDF: ['.pdf'],
    IMAGE: ['.png', '.gif', '.jpg', '.jpeg', '.bmp', '.tif'],
    SPREADSHEET: ['.xls', '.xlsx'],
    DOCUMENT: ['.docx'],
    TEXT: ['.txt'],
    HTML: ['.html'],
    JSON: ['.json'],
    XML: ['.xml'],
    CSV: ['.csv'],
    COMPRESSED: ['.zip', '.tar', '.gz'],
    EML: ['.eml']
};
// extensions that can be emailed but aren't a sub item type
var otherSendableExtensions = ['.doc'];
// the extension that we allow to be sent via email. We don't send compressed as we have unzipped them, same with eml
exports.sendAttachmentExtensions = __spreadArray(__spreadArray(__spreadArray(__spreadArray(__spreadArray(__spreadArray(__spreadArray(__spreadArray(__spreadArray(__spreadArray([], exports.extensionsForSubItemTypes[exports.subItemTypes.PDF]), exports.extensionsForSubItemTypes[exports.subItemTypes.IMAGE]), exports.extensionsForSubItemTypes[exports.subItemTypes.SPREADSHEET]), exports.extensionsForSubItemTypes[exports.subItemTypes.DOCUMENT]), exports.extensionsForSubItemTypes[exports.subItemTypes.TEXT]), exports.extensionsForSubItemTypes[exports.subItemTypes.HTML]), exports.extensionsForSubItemTypes[exports.subItemTypes.JSON]), exports.extensionsForSubItemTypes[exports.subItemTypes.XML]), exports.extensionsForSubItemTypes[exports.subItemTypes.CSV]), otherSendableExtensions);
