"use strict";
exports.__esModule = true;
exports.composeEmailTypes = void 0;
exports.composeEmailTypes = {
    REPLY: 'REPLY',
    REPLY_ALL: 'REPLY_ALL',
    FORWARD: 'FORWARD',
    NEW: 'NEW',
    INDIVIDUAL_RECIPIENTS: 'INDIVIDUAL_RECIPIENTS'
};
