export declare const inputFieldNames: {
    orgName: string;
    inboundDomain: string;
    timezone: string;
    invoiceStartDate: string;
};
export declare const inputHelpTextValues: {
    [x: string]: string;
};
