/**
 * Gets the sub item type
 * @param {string} extension - The file extension
 * @returns {string}
 */
export declare function getSubItemTypeFromFileExtension(extension?: string): any;
