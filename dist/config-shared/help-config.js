"use strict";
var _a;
exports.__esModule = true;
exports.inputHelpTextValues = exports.inputFieldNames = void 0;
exports.inputFieldNames = {
    orgName: 'orgName',
    inboundDomain: 'inboundDomain',
    timezone: 'timezone',
    invoiceStartDate: 'invoiceStartDate'
};
var dummyText = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.';
exports.inputHelpTextValues = (_a = {},
    _a[exports.inputFieldNames.orgName] = 'This is the orgName helpText. Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    _a[exports.inputFieldNames.inboundDomain] = 'This is the inboundDomain helpText. Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    _a[exports.inputFieldNames.timezone] = 'This is the timezone helpText. Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    _a[exports.inputFieldNames.invoiceStartDate] = 'This is the invoiceStartDate helpText. Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    _a);
