"use strict";
exports.__esModule = true;
exports.deliveryDiscountValueIs = exports.addLineTypes = exports.combineSupplierAccounts = exports.dueDateFormulaTypes = exports.statementDuplicateLineActions = exports.paymentMethods = exports.settingTypes = void 0;
exports.settingTypes = {
    subTotalExcludesDelivery: 'subTotalExcludesDelivery',
    subTotalExcludesDiscount: 'subTotalExcludesDiscount',
    allowTaxOnlyInvoice: 'allowTaxOnlyInvoice',
    statementAllowCumulativeBalanceSignChange: 'statementAllowCumulativeBalanceSignChange',
    statementAllowDocTypeSwap: 'statementAllowDocTypeSwap',
    statementSkipLineSum: 'statementSkipLineSum',
    statementDuplicates: 'statementDuplicates',
    statementHasType: 'statementHasType',
    statementHasSupplierAccountNumber: 'statementHasSupplierAccountNumber',
    statementHasDate: 'statementHasDate',
    statementHasUnallocatedBalance: 'statementHasUnallocatedBalance',
    statementHasCumulativeBalance: 'statementHasCumulativeBalance',
    statementHasGrandTotal: 'statementHasGrandTotal',
    statementDateIsFlexible: 'statementDateIsFlexible',
    statementUnchangedNumberSign: 'statementUnchangedNumberSign',
    skipNumberSignCheckForInvoiceLines: 'skipNumberSignCheckForInvoiceLines',
    noTaxShownNotification: 'noTaxShownNotification',
    currency: 'currency',
    paymentMethod: 'paymentMethod',
    dueDateFormula: 'dueDateFormula',
    splitLine: 'splitLine',
    lineDescription: 'lineDescription',
    nominalCode: 'nominalCode',
    // nominalDescription // this is a UI field that is added
    removeTax: 'removeTax',
    combineSupplierAccounts: 'combineSupplierAccounts',
    invoiceStartDate: 'invoiceStartDate',
    invoiceEndDate: 'invoiceEndDate',
    addLineType: 'addLineType',
    withTaxId: 'withTaxId',
    withoutTaxId: 'withoutTaxId',
    trackings: 'trackings',
    exportAsStatus: 'exportAsStatus',
    deliveryTotalLine: 'deliveryTotalLine',
    deliveryTotalIs: 'deliveryTotalIs',
    discountTotalIs: 'discountTotalIs',
    statementUserNotes: 'statementUserNotes',
    matchTolerance: 'matchTolerance',
    statementContactNotes: 'statementContactNotes',
    processDocTypes: 'processDocTypes',
    statementLineNotTracked: 'statementLineNotTracked'
};
exports.paymentMethods = {
    BATCH: 'BATCH',
    DIRECT_DEBIT: 'DIRECT_DEBIT',
    MANUAL: 'MANUAL'
};
exports.statementDuplicateLineActions = {
    IGNORE: 'IGNORE',
    SUM: 'SUM',
    REMOVE: 'REMOVE'
};
exports.dueDateFormulaTypes = {
    INVOICE_DUE_DATE: 'INVOICE_DUE_DATE',
    DAYS_OFFSET: 'DAYS_OFFSET',
    MONTHS_DATE_OFFSET: 'MONTHS_DATE_OFFSET' // { dueDateFormulaType: 'MONTHS_DATE_OFFSET', 'months': 1, 'date':31}
};
exports.combineSupplierAccounts = {
    SITE: 'SITE',
    COMPANY: 'COMPANY',
    ORG: 'ORG'
};
exports.addLineTypes = {
    SINGLE: 'SINGLE',
    PER_TAX: 'PER_TAX' // adds a new line for each tax
};
exports.deliveryDiscountValueIs = {
    SUB_TOTAL: 'SUB_TOTAL',
    GRAND_TOTAL: 'GRAND_TOTAL' // the delivery total includes tax
};
