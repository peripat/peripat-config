"use strict";
exports.__esModule = true;
exports.fieldEditTypes = void 0;
exports.fieldEditTypes = {
    BOOLEAN: 'BOOLEAN',
    STRING_SINGLE_LINE: 'STRING_SINGLE_LINE',
    STRING_MULTI_LINE: 'STRING_MULTI_LINE',
    SELECT: 'SELECT',
    NUMBER: 'NUMBER',
    DATE: 'DATE',
    DUE_DATE: 'DUE_DATE',
    SINGLE_STRING_ARRAY: 'SINGLE_STRING_ARRAY',
    SELECT_MULTI: 'SELECT_MULTI',
    SELECT_USER_INPUT: 'SELECT_USER_INPUT',
    JSON: 'JSON',
    TIMEZONE: 'TIMEZONE' // created as a type because of the amount of data being passed from backend to ui
};
