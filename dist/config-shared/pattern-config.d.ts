export declare const patternAdminActions: {
    RUN_REGEX: string;
    RUN_PATTERN: string;
    UPDATE_OBJECTS: string;
    RUN_TEST_SUB_ITEMS: string;
};
