/**
 * Gets the object type from either the id given or the object._id. It is best to provide
 * the object which has an _id to work out what the object type is. The other methods are if
 * they have to be used
 * @param {(string|object)} id - this can be a string or an object with a ._id field
 * @param {object} object - OPTIONAL. This can be provided to help determin if an id.
 * @returns {string}
 */
export declare function getObjectTypeFromId(id: any, object: any): any;
