export declare const settingTypes: {
    subTotalExcludesDelivery: string;
    subTotalExcludesDiscount: string;
    allowTaxOnlyInvoice: string;
    statementAllowCumulativeBalanceSignChange: string;
    statementAllowDocTypeSwap: string;
    statementSkipLineSum: string;
    statementDuplicates: string;
    statementHasType: string;
    statementHasSupplierAccountNumber: string;
    statementHasDate: string;
    statementHasUnallocatedBalance: string;
    statementHasCumulativeBalance: string;
    statementHasGrandTotal: string;
    statementDateIsFlexible: string;
    statementUnchangedNumberSign: string;
    skipNumberSignCheckForInvoiceLines: string;
    noTaxShownNotification: string;
    currency: string;
    paymentMethod: string;
    dueDateFormula: string;
    splitLine: string;
    lineDescription: string;
    nominalCode: string;
    removeTax: string;
    combineSupplierAccounts: string;
    invoiceStartDate: string;
    invoiceEndDate: string;
    addLineType: string;
    withTaxId: string;
    withoutTaxId: string;
    trackings: string;
    exportAsStatus: string;
    deliveryTotalLine: string;
    deliveryTotalIs: string;
    discountTotalIs: string;
    statementUserNotes: string;
    matchTolerance: string;
    statementContactNotes: string;
    processDocTypes: string;
    statementLineNotTracked: string;
};
export declare const paymentMethods: {
    BATCH: string;
    DIRECT_DEBIT: string;
    MANUAL: string;
};
export declare const statementDuplicateLineActions: {
    IGNORE: string;
    SUM: string;
    REMOVE: string;
};
export declare const dueDateFormulaTypes: {
    INVOICE_DUE_DATE: string;
    DAYS_OFFSET: string;
    MONTHS_DATE_OFFSET: string;
};
export declare const combineSupplierAccounts: {
    SITE: string;
    COMPANY: string;
    ORG: string;
};
export declare const addLineTypes: {
    SINGLE: string;
    PER_TAX: string;
};
export declare const deliveryDiscountValueIs: {
    SUB_TOTAL: string;
    GRAND_TOTAL: string;
};
