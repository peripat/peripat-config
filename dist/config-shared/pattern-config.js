"use strict";
exports.__esModule = true;
exports.patternAdminActions = void 0;
exports.patternAdminActions = {
    RUN_REGEX: 'RUN_REGEX',
    RUN_PATTERN: 'RUN_PATTERN',
    UPDATE_OBJECTS: 'UPDATE_OBJECTS',
    RUN_TEST_SUB_ITEMS: 'RUN_TEST_SUB_ITEMS'
};
