export declare const inboxItemActions: {
    CREATE_NOTIFICATION: string;
    MARK_AS_CHECKED: string;
    MARK_AS_NOT_CHECKED: string;
    MARK_AS_VIEWED: string;
    MARK_AS_NOT_VIEWED: string;
    CLEAR_INBOX_ITEM_NOTIFICATIONS: string;
};
export declare const inboxItemActionIsBulk: {
    CREATE_NOTIFICATION: boolean;
    MARK_AS_CHECKED: boolean;
    MARK_AS_NOT_CHECKED: boolean;
    MARK_AS_VIEWED: boolean;
    MARK_AS_NOT_VIEWED: boolean;
    CLEAR_INBOX_ITEM_NOTIFICATIONS: boolean;
};
