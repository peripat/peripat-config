export const settingTypes = {
  subTotalExcludesDelivery: 'subTotalExcludesDelivery',
  subTotalExcludesDiscount: 'subTotalExcludesDiscount',
  allowTaxOnlyInvoice: 'allowTaxOnlyInvoice',
  statementAllowCumulativeBalanceSignChange: 'statementAllowCumulativeBalanceSignChange',
  statementAllowDocTypeSwap: 'statementAllowDocTypeSwap',
  statementSkipLineSum: 'statementSkipLineSum',
  statementDuplicates: 'statementDuplicates',
  statementHasType: 'statementHasType',
  statementHasSupplierAccountNumber: 'statementHasSupplierAccountNumber',
  statementHasDate: 'statementHasDate',
  statementHasUnallocatedBalance: 'statementHasUnallocatedBalance',
  statementHasCumulativeBalance: 'statementHasCumulativeBalance',
  statementHasGrandTotal: 'statementHasGrandTotal',
  statementDateIsFlexible: 'statementDateIsFlexible',
  statementUnchangedNumberSign: 'statementUnchangedNumberSign',
  skipNumberSignCheckForInvoiceLines: 'skipNumberSignCheckForInvoiceLines',
  noTaxShownNotification: 'noTaxShownNotification',
  currency: 'currency',
  paymentMethod: 'paymentMethod',
  dueDateFormula: 'dueDateFormula',
  splitLine: 'splitLine',
  lineDescription: 'lineDescription',
  nominalCode: 'nominalCode',
  // nominalDescription // this is a UI field that is added
  removeTax: 'removeTax',
  combineSupplierAccounts: 'combineSupplierAccounts',
  invoiceStartDate: 'invoiceStartDate',
  invoiceEndDate: 'invoiceEndDate',
  addLineType: 'addLineType',
  withTaxId: 'withTaxId',
  withoutTaxId: 'withoutTaxId',
  trackings: 'trackings',
  exportAsStatus: 'exportAsStatus',
  deliveryTotalLine: 'deliveryTotalLine',
  deliveryTotalIs: 'deliveryTotalIs', // either SUB_TOTAL or GRAND_TOTAL. Used when we are detecting lines an add deliveryTotal as a line during cleanup
  discountTotalIs: 'discountTotalIs', // either SUB_TOTAL or GRAND_TOTAL. Used when we are detecting lines an add deliveryTotal as a line during cleanup
  statementUserNotes: 'statementUserNotes',
  matchTolerance: 'matchTolerance',
  statementContactNotes: 'statementContactNotes',
  processDocTypes: 'processDocTypes',
  statementLineNotTracked: 'statementLineNotTracked'
};

export const paymentMethods = {
  BATCH: 'BATCH',
  DIRECT_DEBIT: 'DIRECT_DEBIT',
  MANUAL: 'MANUAL'
};

export const statementDuplicateLineActions = {
  IGNORE: 'IGNORE',
  SUM: 'SUM',
  REMOVE: 'REMOVE'
};

export const dueDateFormulaTypes = {
  INVOICE_DUE_DATE: 'INVOICE_DUE_DATE', // { dueDateFormulaType: 'INVOICE_DUE_DATE'}
  DAYS_OFFSET: 'DAYS_OFFSET', // { dueDateFormulaType: 'DAYS_OFFSET', 'days': 30}
  MONTHS_DATE_OFFSET: 'MONTHS_DATE_OFFSET' // { dueDateFormulaType: 'MONTHS_DATE_OFFSET', 'months': 1, 'date':31}
};

export const combineSupplierAccounts = {
  SITE: 'SITE',
  COMPANY: 'COMPANY',
  ORG: 'ORG'
};

export const addLineTypes = {
  SINGLE: 'SINGLE', // adds a single line even if taxes are mixed (eg some lines with 0% and 20%)
  PER_TAX: 'PER_TAX' // adds a new line for each tax
};

export const deliveryDiscountValueIs = {
  SUB_TOTAL: 'SUB_TOTAL', // the delivery total excludes tax
  GRAND_TOTAL: 'GRAND_TOTAL' // the delivery total includes tax
};
