"use strict";
var _a;
exports.__esModule = true;
exports.docMatchStatuses = exports.deliveryConfirmationTypes = exports.missingInvoiceStatusesOutstanding = exports.missingInvoiceStatuses = exports.reconciledStatuses = exports.completedDocApprovalStatuses = exports.docApprovalStatuses = exports.docStatuses = exports.notRunAccountingExportResults = exports.accountingExportResults = exports.completedDocCaptureResults = exports.docCaptureResults = exports.docCaptureStatuses = void 0;
exports.docCaptureStatuses = {
    NEW: 'NEW',
    CAPTURED: 'CAPTURED',
    CLEANED: 'CLEANED',
    ORG_APPLIED: 'ORG_APPLIED',
    RESULT: 'RESULT'
};
exports.docCaptureResults = {
    RECORDED: 'RECORDED',
    DUPLICATE: 'DUPLICATE',
    SKIPPED: 'SKIPPED',
    NOTIFICATION: 'NOTIFICATION',
    DELETED: 'DELETED'
};
// the results which means that the doc capture is completed. Basically everything except notification
exports.completedDocCaptureResults = [
    exports.docCaptureResults.RECORDED,
    exports.docCaptureResults.DUPLICATE,
    exports.docCaptureResults.DELETED,
    exports.docCaptureResults.SKIPPED
];
exports.accountingExportResults = {
    AWAITING_EXPORT: 'AWAITING_EXPORT',
    TEXT_DONE: 'TEXT_DONE',
    ATTACHMENT_DONE: 'ATTACHMENT_DONE',
    EXPORTED: 'EXPORTED',
    ERROR: 'ERROR',
    RETRY: 'RETRY',
    EXPORT_SKIPPED: 'EXPORT_SKIPPED'
};
// do not run the export for the following export results
exports.notRunAccountingExportResults = [
    exports.accountingExportResults.EXPORTED,
    exports.accountingExportResults.ERROR,
    exports.accountingExportResults.EXPORT_SKIPPED
];
exports.docStatuses = {
    CAPTURE_INCOMPLETE: 'CAPTURE_INCOMPLETE',
    APPROVAL_INCOMPLETE: 'APPROVAL_INCOMPLETE',
    EXPORT_INCOMPLETE: 'EXPORT_INCOMPLETE',
    PROCESSING_COMPLETE: 'PROCESSING_COMPLETE',
    UNKNOWN: 'UNKNOWN'
};
exports.docApprovalStatuses = {
    AWAITING_APPROVAL: 'AWAITING_APPROVAL',
    PART_APPROVED: 'PART_APPROVED',
    REJECTED: 'REJECTED',
    APPROVED: 'APPROVED' // completely approved
};
exports.completedDocApprovalStatuses = [
    exports.docApprovalStatuses.REJECTED,
    exports.docApprovalStatuses.APPROVED
];
// reconciled status options for statement lines
exports.reconciledStatuses = (_a = {
        PROCESSED: 'PROCESSED',
        MISSING: 'MISSING'
    },
    _a['N/A'] = 'N/A',
    _a);
// these are the statuses for tracked missing invoices.
exports.missingInvoiceStatuses = {
    NEW: 'NEW',
    REPORT_SENT: 'REPORT_SENT',
    REMINDER_1_SENT: 'REMINDER_1_SENT',
    REMINDER_2_SENT: 'REMINDER_2_SENT',
    USER_NOTIFICATION_CREATED: 'USER_NOTIFICATION_CREATED',
    TRACKING_EXPIRED: 'TRACKING_EXPIRED',
    STOPPED_TRACKING: 'STOPPED_TRACKING',
    INVOICE_RECEIVED: 'INVOICE_RECEIVED'
};
// the statuses where we are still waiting for the invoice to come in. The others should have isTracked=false anyway
exports.missingInvoiceStatusesOutstanding = [
    exports.missingInvoiceStatuses.NEW,
    exports.missingInvoiceStatuses.REPORT_SENT,
    exports.missingInvoiceStatuses.REMINDER_1_SENT,
    exports.missingInvoiceStatuses.REMINDER_2_SENT,
    exports.missingInvoiceStatuses.USER_NOTIFICATION_CREATED
];
exports.deliveryConfirmationTypes = {
    INVOICE: 'INVOICE',
    DELIVERY_NOTE: 'DELIVERY_NOTE',
    PURCHASE_ORDER: 'PURCHASE_ORDER'
};
exports.docMatchStatuses = {
    AWAITING_MATCH: 'AWAITING_MATCH',
    PART_MATCH: 'PART_MATCH',
    MATCHED: 'MATCHED',
    OVER_MATCHED: 'OVER_MATCHED'
};
