export declare const parentContactTypes: {
    ACCOUNTING: string;
    COMPANY_GROUP: string;
    PAYMENTS: string;
    DELIVERIES: string;
    FINANCE: string;
    ASSET_MANAGEMENT: string;
};
export declare const capturedItemTypes: {
    PATTERN: string;
    FIELD: string;
};
