"use strict";
exports.__esModule = true;
exports.getObjectTypeFromId = void 0;
var config_shared_1 = require("../config-shared");
/**
 * Gets the object type from either the id given or the object._id. It is best to provide
 * the object which has an _id to work out what the object type is. The other methods are if
 * they have to be used
 * @param {(string|object)} id - this can be a string or an object with a ._id field
 * @param {object} object - OPTIONAL. This can be provided to help determin if an id.
 * @returns {string}
 */
function getObjectTypeFromId(id, object) {
    if (!id)
        return;
    var hasOrgId;
    if (typeof id === 'string' && object) {
        hasOrgId = !!object.orgId; // stores if the object has a orgId
    }
    if (typeof id === 'object') {
        hasOrgId = !!id.orgId; // stores if the object has a orgId
        id = id._id;
    }
    if (typeof id !== 'string')
        return;
    var idPrefix = id.substring(0, 3);
    var objectType = Object.keys(config_shared_1.objectTypeIdPrefixes).find(function (key) { return config_shared_1.objectTypeIdPrefixes[key] === idPrefix; });
    // if it is a contact and we had the object earlier, determine whether it is a global or org contact
    if (objectType === config_shared_1.objectTypes.ORG_CONTACT && hasOrgId === false) {
        objectType = config_shared_1.objectTypes.GLOBAL_CONTACT;
    }
    else if (objectType === config_shared_1.objectTypes.GLOBAL_CONTACT && hasOrgId === true) {
        objectType = config_shared_1.objectTypes.ORG_CONTACT;
    }
    return objectType;
}
exports.getObjectTypeFromId = getObjectTypeFromId;
