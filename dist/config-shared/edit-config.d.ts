export declare const fieldEditTypes: {
    BOOLEAN: string;
    STRING_SINGLE_LINE: string;
    STRING_MULTI_LINE: string;
    SELECT: string;
    NUMBER: string;
    DATE: string;
    DUE_DATE: string;
    SINGLE_STRING_ARRAY: string;
    SELECT_MULTI: string;
    SELECT_USER_INPUT: string;
    JSON: string;
    TIMEZONE: string;
};
