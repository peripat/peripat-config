To compile this typescript package run:

npx typescript src/* --declaration --outDir dist

These errors will appear but the dist folder has been compiled (need to look into this) :

npx: installed 1 in 1.341s
error TS6231: Could not resolve the path 'src/config-shared' with the extensions: '.ts', '.tsx', '.d.ts'.

error TS6231: Could not resolve the path 'src/config-ui' with the extensions: '.ts', '.tsx', '.d.ts'.

see https://www.carlrippon.com/installing-and-running-the-typescript-compiler/ for more info on the compiler

# Rob's Steps
* Make changes to config project
* check if you need to update the index.js within the src/config-shared (or config-ui) folder if there is a new file
* Compile this typescript package. Run `npx -p typescript tsc src/* --declaration --outDir dist` in console for the config project (2 errors will occur TS6231)
* Previously we were running  'npx typescript src/* --declaration --outDir dist' but it stopped working
* Comit the changes and push to bitbucket
* Open bitbucket for the config project, open comits, then click the link for the comit you have just done
* Copy the comit id from the URL. It will be like '2e56a73e8d12357d405874f38f4afd703b6019c6'
* Open the package.json for the AP and UI projects. Paste the comit id into the file against the config project
* Run `npm i git+https:://rob_cassidy@bitbucket.org/peripat/peripat-config.git#2e56a73e8d12357d405874f38f4afd703b6019c6` for both AP and UI
or `npm update peripat-config`
projects (replacing the commit ID)
