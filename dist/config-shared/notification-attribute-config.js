"use strict";
exports.__esModule = true;
exports.notificationAssignedTos = exports.notificationSeverities = void 0;
exports.notificationSeverities = {
    MESSAGE: 'MESSAGE',
    WARNING: 'WARNING',
    ACTION_REQUIRED: 'ACTION_REQUIRED',
    ERROR: 'ERROR'
};
exports.notificationAssignedTos = {
    ORG: 'ORG',
    ADMIN: 'ADMIN'
};
