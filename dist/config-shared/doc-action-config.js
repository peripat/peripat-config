"use strict";
exports.__esModule = true;
exports.manualInvoiceActions = exports.docActionIsBulk = exports.docActions = void 0;
exports.docActions = {
    APPROVE: 'APPROVE',
    REJECT: 'REJECT',
    REJECT_DELETE: 'REJECT_DELETE',
    DELETE: 'DELETE',
    RESET_APPROVALS: 'RESET_APPROVALS'
};
exports.docActionIsBulk = {
    APPROVE: true,
    REJECT: true,
    REJECT_DELETE: true,
    DELETE: true,
    RESET_APPROVALS: true
};
exports.manualInvoiceActions = {
    CREATE_MANUAL_INVOICE: 'CREATE_MANUAL_INVOICE'
};
