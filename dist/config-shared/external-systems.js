"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.externalSystemSettingsNames = exports.externalSystems = exports.orderingSystems = exports.accountingSystems = void 0;
exports.accountingSystems = {
    XERO: 'XERO',
    SAGE: 'SAGE',
    QUICKBOOKS: 'QUICKBOOKS',
    FREEAGENT: 'FREEAGENT',
    NETSUITE: 'NETSUITE',
    KASHFLOW: 'KASHFLOW',
    TWINFIELD: 'TWINFIELD',
    AQUILLA: 'AQUILLA',
    // if anything is added, also update in externalSystemNameMap external-system-name-map.js
    OTHER_ACCOUNTING: 'OTHER_ACCOUNTING'
};
exports.orderingSystems = {
    MARKETMAN: 'MARKETMAN',
    TEVALIS: 'TEVALIS',
    FNB_SHOP: 'FNB_SHOP',
    PRONETT: 'PRONETT',
    OTHER_ORDERING: 'OTHER_ORDERING'
};
exports.externalSystems = __assign(__assign({ EMAIL: 'EMAIL' }, exports.accountingSystems), exports.orderingSystems);
exports.externalSystemSettingsNames = {
    XERO: 'xeroSettings',
    SAGE: 'sageSettings',
    QUICKBOOKS: 'quickbooksSettings',
    FREEAGENT: 'freeagentSettings',
    NETSUITE: 'netsuiteSettings',
    KASHFLOW: 'kashflowSettings',
    TWINFIELD: 'twinfieldSettings',
    AQUILLA: 'aquillaSettings',
    OTHER_ACCOUNTING: 'otherAccountingSettings',
    MARKETMAN: 'marketmanSettings',
    TEVALIS: 'tevalisSettings',
    FNB_SHOP: 'fnbShopSettings',
    PRONETT: 'pronettSettings'
};
