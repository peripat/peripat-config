export declare const docCaptureStatuses: {
    NEW: string;
    CAPTURED: string;
    CLEANED: string;
    ORG_APPLIED: string;
    RESULT: string;
};
export declare const docCaptureResults: {
    RECORDED: string;
    DUPLICATE: string;
    SKIPPED: string;
    NOTIFICATION: string;
    DELETED: string;
};
export declare const completedDocCaptureResults: string[];
export declare const accountingExportResults: {
    AWAITING_EXPORT: string;
    TEXT_DONE: string;
    ATTACHMENT_DONE: string;
    EXPORTED: string;
    ERROR: string;
    RETRY: string;
    EXPORT_SKIPPED: string;
};
export declare const notRunAccountingExportResults: string[];
export declare const docStatuses: {
    CAPTURE_INCOMPLETE: string;
    APPROVAL_INCOMPLETE: string;
    EXPORT_INCOMPLETE: string;
    PROCESSING_COMPLETE: string;
    UNKNOWN: string;
};
export declare const docApprovalStatuses: {
    AWAITING_APPROVAL: string;
    PART_APPROVED: string;
    REJECTED: string;
    APPROVED: string;
};
export declare const completedDocApprovalStatuses: string[];
export declare const reconciledStatuses: {
    PROCESSED: string;
    MISSING: string;
    "N/A": string;
};
export declare const missingInvoiceStatuses: {
    NEW: string;
    REPORT_SENT: string;
    REMINDER_1_SENT: string;
    REMINDER_2_SENT: string;
    USER_NOTIFICATION_CREATED: string;
    TRACKING_EXPIRED: string;
    STOPPED_TRACKING: string;
    INVOICE_RECEIVED: string;
};
export declare const missingInvoiceStatusesOutstanding: string[];
export declare const deliveryConfirmationTypes: {
    INVOICE: string;
    DELIVERY_NOTE: string;
    PURCHASE_ORDER: string;
};
export declare const docMatchStatuses: {
    AWAITING_MATCH: string;
    PART_MATCH: string;
    MATCHED: string;
    OVER_MATCHED: string;
};
