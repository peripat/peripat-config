"use strict";
// see also postmark-config for postmark specific config
exports.__esModule = true;
exports.sentEmailStatuses = void 0;
exports.sentEmailStatuses = {
    AWAITING_DELIVERY: 'AWAITING_DELIVERY',
    DELIVERED: 'DELIVERED',
    UNLINKED: 'UNLINKED' // An email that has come in via the delivery/bounce hook but we couldn't find a corresponding email to link it to (might not have been stored yet)
};
