export const orgStatuses = {
  ACTIVE: 'ACTIVE',
  ARCHIVED: 'ARCHIVED',
  DRAFT: 'DRAFT',
  CLOSED: 'CLOSED'
};

export const inboxStatuses = {
  ...orgStatuses,
  USER_HOLD: 'USER_HOLD', // user can switch off the hold
  ADMIN_HOLD: 'ADMIN_HOLD' // only admins can switch off the hold
};
