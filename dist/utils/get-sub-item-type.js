"use strict";
exports.__esModule = true;
exports.getSubItemTypeFromFileExtension = void 0;
var config_shared_1 = require("../config-shared");
/**
 * Gets the sub item type
 * @param {string} extension - The file extension
 * @returns {string}
 */
function getSubItemTypeFromFileExtension(extension) {
    if (extension === void 0) { extension = ''; }
    var ext = extension.toLowerCase();
    var subItemType = Object.keys(config_shared_1.extensionsForSubItemTypes).find(function (key) {
        return config_shared_1.extensionsForSubItemTypes[key].includes(ext);
    });
    return subItemType || config_shared_1.subItemTypes.UNKNOWN_FILE;
}
exports.getSubItemTypeFromFileExtension = getSubItemTypeFromFileExtension;
