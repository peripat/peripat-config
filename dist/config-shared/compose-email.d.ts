export declare const composeEmailTypes: {
    REPLY: string;
    REPLY_ALL: string;
    FORWARD: string;
    NEW: string;
    INDIVIDUAL_RECIPIENTS: string;
};
