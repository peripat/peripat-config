export const ruleExecutionPoints = {
  PRE_CAPTURE_INBOX_ITEM: 'PRE_CAPTURE_INBOX_ITEM', // run before #1 is run
  PRE_CAPTURE_SUB_ITEM: 'PRE_CAPTURE_SUB_ITEM', // run before #1 is run
  CAPTURE_DOC: 'CAPTURE_DOC', // in #2
  CLEANUP_DOC: 'CLEANUP_DOC', // in #3
  APPLY_TO_DOC: 'APPLY_TO_DOC', // in #4
  DOC_RESULT: 'DOC_RESULT', // in #5
  NO_MATCH_ON_SUB_ITEM: 'NO_MATCH_ON_SUB_ITEM', // used for running rules to dismiss pdfs like pronett payment certificates
  SENDER_UNKNOWN: 'SENDER_UNKNOWN'
};

export const ruleActionTypes = {
  SET_VALUE: 'SET_VALUE',
  SET_UPPER_CASE: 'SET_UPPER_CASE',
  SET_LOWER_CASE: 'SET_LOWER_CASE',
  REPLACE_STR: 'REPLACE_STR',
  APPEND_STR: 'APPEND_STR',
  CREATE_NOTIFICATION: 'CREATE_NOTIFICATION',
  THROW_ERROR: 'THROW_ERROR',
  CAPTURE_FIELD: 'CAPTURE_FIELD',
  ADD_TRACKING: 'ADD_TRACKING'
};
