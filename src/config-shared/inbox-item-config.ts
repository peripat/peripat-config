// also update inbox-item-name-map.js

export const inboxItemResults = {
  PROCESSED: 'PROCESSED',
  INCOMPLETE: 'INCOMPLETE',
  NOTIFICATION: 'NOTIFICATION',
  ON_HOLD: 'ON_HOLD'
};

export const postmarkResults = {
  NEW: 'NEW',
  DONE: 'DONE',
  NOT_RUN: 'NOT_RUN', // This is not set by the code
  ERROR: 'ERROR' // this is set but should only be temp state. Will need to be switched to NOT_RUN manually if it is not going to be run
};

export const inboxItemTypes = {
  EMAIL: 'EMAIL',
  FILE: 'FILE'
};
