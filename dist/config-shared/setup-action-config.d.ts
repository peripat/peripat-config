export declare const setupActions: {
    CREATE_NEW_ORG: string;
    CREATE_NEW_COMPANY: string;
    CREATE_NEW_SITE: string;
    CREATE_NEW_INBOX: string;
};
