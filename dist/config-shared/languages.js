"use strict";
exports.__esModule = true;
exports.languageProperties = exports.languages = void 0;
exports.languages = {
    en: 'en',
    es: 'es',
    fr: 'fr',
    it: 'it',
    de: 'de',
    pt: 'pt'
};
exports.languageProperties = [
    { language: exports.languages.en, languageName: 'English' },
    { language: exports.languages.fr, languageName: 'French' },
    { language: exports.languages.de, languageName: 'German' },
    { language: exports.languages.it, languageName: 'Italian' },
    { language: exports.languages.pt, languageName: 'Portuguese' },
    { language: exports.languages.es, languageName: 'Spanish' }
];
