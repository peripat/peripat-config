import * as moment from 'moment-timezone';
import { regions, accountingSystems } from '../config-shared';
const timezones = moment.tz.names();

/**
 * adds the escape characters to a regex. See https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
 * @param {string} regexText - The string that should be intepreted literally, eg . should be \.
 * @returns {string} - regexText with escape characters added
 */
function escapeRegExp(regexText) {
    return regexText.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
}

// @ts-ignore
export function checkInboxAgainstOrg(newInbox, org) {
    let errors = [];

    let existing = org.inboxes.find(
        inbox => inbox.inboxName.toUpperCase() === newInbox.inboxName.toUpperCase()
    );
    if (existing) {
        errors.push(
            `An inbox named '${existing.inboxName}' already exists for organisation '${org.orgName}'`
        );
    }

    existing = org.inboxes.find(
        inbox => inbox.inboundEmail.toLowerCase() === newInbox.inboundEmail.toLowerCase()
    );
    if (existing) {
        errors.push(
            `An inbox with inbound email address '${existing.inboundEmail}' already exists for organisation '${org.orgName}'`
        );
    }

    existing = org.inboxes.find(
        inbox => inbox.outboundEmail.toLowerCase() === newInbox.outboundEmail.toLowerCase()
    );
    if (existing) {
        errors.push(
            `An inbox with outbound email address '${existing.outboundEmail}' already exists for organisation '${org.orgName}'`
        );
    }

    let regexStr = `^[a-z\\d\\-\\.\\_\\+]+@${escapeRegExp(org.inboundDomain)}$`;
    let regex = new RegExp(regexStr);
    if (!regex.test(newInbox.inboundEmail)) {
        let error = `Inbound email '${newInbox.inboundEmail}' is invalid. It must be lower case, digits or '.' '-' '_' '+' characters and must end with '@${org.inboundDomain}'`;
        errors.push(error);
    }

    return errors;
}

export function sanitiseAndCheckCompanyAttributes(company) {
    let errors = [];
    if (!company) {
        errors.push(`company attributes missing`);
        return errors; // will error if it goes further
    }
    if (!company.orgId) {
        errors.push(`Org ID missing`);
    }

    // check the companyName is valid
    if (typeof company.companyName === 'string') {
        company.companyName = company.companyName.trim().replace(/ {2,}/g, ' ');
        if (company.companyName.length < 3) {
            errors.push(`company name '${company.companyName}' must be at least 3 characters`);
        }
    } else {
        errors.push(`company name must be provided`);
    }

    // check the accountingSystem is valid
    if (typeof company.accountingSystem === 'string') {
        if (!Object.keys(accountingSystems).includes(company.accountingSystem)) {
            errors.push(`Unknown accounting system '${company.accountingSystem}'`);
        }
    } else {
        errors.push(`accounting system must be provided`);
    }

    // sanitise the company number
    if (company.companyNumber) {
        company.companyNumber = company.companyNumber.replace(/\s+/g, '').toUpperCase();
    }

    // sanitise the tax number
    if (company.taxNumber) {
        company.taxNumber = company.taxNumber.replace(/\s+/g, '').toUpperCase();
    }

    // check the region is valid
    if (typeof company.region === 'string') {
        if (!Object.keys(regions).includes(company.region)) {
            errors.push(`Unknown region '${company.region}'`);
        }
    } else {
        errors.push(`region must be provided`);
    }

    // check the timezone is valid
    if (typeof company.timezone === 'string') {
        if (!timezones.includes(company.timezone)) {
            errors.push(`timezone '${company.timezone}' is not valid`);
        }
    }

    // check the invoiceStartDate is valid
    if (company.settings && company.settings.invoiceStartDate) {
        if (moment(company.settings.invoiceStartDate).isValid()) {
            let invoiceStart = moment.utc(company.settings.invoiceStartDate).startOf('day');
            let minus1Year = moment().subtract(53, 'weeks'); // 53 weeks to get around the timezone stuff
            let add6Months = moment().add(6, 'months');
            if (!invoiceStart.isBetween(minus1Year, add6Months)) {
                let error = `start processing from date must be between 1 year ago and 6 months in the future`;
                errors.push(error);
            }
            company.settings.invoiceStartDate = invoiceStart.toDate();
        } else {
            errors.push(`start processing from date is not a valid date`);
        }
    }

    return errors;
}

export function sanitiseAndCheckSiteAttributes(site) {
    let errors = [];
    if (!site) {
        errors.push(`site attributes missing`);
        return errors; // will error if it goes further
    }
    if (!site.orgId) errors.push(`Org ID missing`);
    if (!site.companyId) errors.push(`Company ID missing`);

    // check the siteName is valid
    if (typeof site.siteName === 'string') {
        site.siteName = site.siteName.trim().replace(/ {2,}/g, ' ');
        if (site.siteName.length < 3) {
            errors.push(`site name '${site.siteName}' must be at least 3 characters`);
        }
    } else {
        errors.push(`site name must be provided`);
    }

    // check the timezone is valid
    if (typeof site.timezone === 'string') {
        if (!timezones.includes(site.timezone)) {
            errors.push(`timezone '${site.timezone}' is not valid`);
        }
    }

    // check the invoiceStartDate is valid
    if (site.settings && site.settings.invoiceStartDate) {
        if (moment(site.settings.invoiceStartDate).isValid()) {
            let invoiceStart = moment.utc(site.settings.invoiceStartDate).startOf('day');
            let minus1Year = moment().subtract(53, 'weeks'); // 53 weeks to get around the timezone stuff
            let add6Months = moment().add(6, 'months');
            if (!invoiceStart.isBetween(minus1Year, add6Months)) {
                let error = `start processing from date must be between 1 year ago and 6 months in the future`;
                errors.push(error);
            }
            site.settings.invoiceStartDate = invoiceStart.toDate();
        } else {
            errors.push(`start processing from date is not a valid date`);
        }
    }

    return errors;
}

export function checkSiteAgainstOrg(newSite, org, company) {
    let errors = [];

    let existingSite;
    org.companies.forEach(company =>
        company.sites.forEach(site => {
            if (site.siteName.toUpperCase() === newSite.siteName.toUpperCase()) {
                existingSite = site;
            }
        })
    );
    if (existingSite) {
        errors.push(
            `A site named '${existingSite.siteName}' already exists for organisation '${org.orgName}'`
        );
    }

    return errors;
}

export function sanitiseAndCheckInboxAttributes(inbox) {
    let errors = [];
    if (!inbox) {
        errors.push(`inbox attributes missing`);
        return errors; // will error if it goes further
    }
    if (!inbox.orgId) {
        errors.push(`Organisation ID missing`);
    }

    // check the inboxName is valid
    if (typeof inbox.inboxName === 'string') {
        inbox.inboxName = inbox.inboxName.trim().replace(/ {2,}/g, ' ');
        if (inbox.inboxName.length < 3) {
            errors.push(`inbox name '${inbox.inboxName}' must be at least 3 characters`);
        }
    } else {
        errors.push(`inbox name must be provided`);
    }

    if (typeof inbox.outboundEmail === 'string') {
        inbox.outboundEmail = inbox.outboundEmail.toLowerCase();
        if (inbox.outboundEmail.length < 1) {
            errors.push(`outbound email '${inbox.outboundEmail}' must be at least 1 character`);
        }
        if (/^[a-z]+[a-z\d\-\.\_\+]*@[a-z\d\-\.]+$/.test(inbox.outboundEmail) !== true) {
            errors.push(`outbound email '${inbox.outboundEmail}' is invalid`);
        }
    } else {
        errors.push(`outbound email must be provided`);
    }

    if (typeof inbox.inboundEmail === 'string') {
        inbox.inboundEmail = inbox.inboundEmail.toLowerCase();
        if (inbox.inboundEmail.length < 1) {
            errors.push(`inbound email '${inbox.inboundEmail}' must be at least 1 character`);
        }
        if (/^[a-z]+[a-z\d\-\.\_]*@[a-z\d\-\.]+$/.test(inbox.inboundEmail) !== true) {
            errors.push(`inbound email '${inbox.inboundEmail}' is invalid`);
        }
    } else {
        errors.push(`inbound email must be provided`);
    }

    return errors;
}
