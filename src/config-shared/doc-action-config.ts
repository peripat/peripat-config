export const docActions = {
  APPROVE: 'APPROVE',
  REJECT: 'REJECT',
  REJECT_DELETE: 'REJECT_DELETE',
  DELETE: 'DELETE',
  RESET_APPROVALS: 'RESET_APPROVALS'
};

export const docActionIsBulk = {
  APPROVE: true,
  REJECT: true,
  REJECT_DELETE: true,
  DELETE: true,
  RESET_APPROVALS: true
};

export const manualInvoiceActions = {
  CREATE_MANUAL_INVOICE: 'CREATE_MANUAL_INVOICE'
};
