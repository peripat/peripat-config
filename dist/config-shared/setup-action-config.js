"use strict";
exports.__esModule = true;
exports.setupActions = void 0;
exports.setupActions = {
    CREATE_NEW_ORG: 'CREATE_NEW_ORG',
    CREATE_NEW_COMPANY: 'CREATE_NEW_COMPANY',
    CREATE_NEW_SITE: 'CREATE_NEW_SITE',
    CREATE_NEW_INBOX: 'CREATE_NEW_INBOX'
};
