export declare function checkInboxAgainstOrg(newInbox: any, org: any): any[];
export declare function sanitiseAndCheckCompanyAttributes(company: any): any[];
export declare function sanitiseAndCheckSiteAttributes(site: any): any[];
export declare function checkSiteAgainstOrg(newSite: any, org: any, company: any): any[];
export declare function sanitiseAndCheckInboxAttributes(inbox: any): any[];
