"use strict";
exports.__esModule = true;
exports.dismissTypeDisplayNames = exports.dismissTypes = exports.statementLineDocTypeNameMap = exports.docTypeDisplayName = exports.lineTypeForDocType = exports.childTypesForDocType = exports.lineTypes = exports.docProcessingOrder = exports.docTypes = exports.bulkDocActions = void 0;
exports.bulkDocActions = [];
exports.docTypes = {
    INVOICE: 'INVOICE',
    CREDIT_NOTE: 'CREDIT_NOTE',
    MANUAL_INVOICE: 'MANUAL_INVOICE',
    MANUAL_CREDIT_NOTE: 'MANUAL_CREDIT_NOTE',
    STATEMENT: 'STATEMENT',
    DELIVERY_CONFIRMATION: 'DELIVERY_CONFIRMATION',
    DISMISS: 'DISMISS'
};
// unknown will be -1 if doing indexOf
exports.docProcessingOrder = [
    exports.docTypes.STATEMENT,
    exports.docTypes.DISMISS,
    exports.docTypes.DELIVERY_CONFIRMATION,
    exports.docTypes.MANUAL_CREDIT_NOTE,
    exports.docTypes.MANUAL_INVOICE,
    exports.docTypes.CREDIT_NOTE,
    exports.docTypes.INVOICE // first
];
exports.lineTypes = {
    INVOICE_LINE: 'INVOICE_LINE',
    CREDIT_NOTE_LINE: 'CREDIT_NOTE_LINE',
    STATEMENT_INVOICE: 'STATEMENT_INVOICE',
    STATEMENT_CREDIT_NOTE: 'STATEMENT_CREDIT_NOTE',
    STATEMENT_PAYMENT: 'STATEMENT_PAYMENT',
    MANUAL_INVOICE_LINE: 'MANUAL_INVOICE_LINE',
    MANUAL_CREDIT_NOTE_LINE: 'MANUAL_CREDIT_NOTE_LINE',
    DELIVERY_CONFIRMATION_LINE: 'DELIVERY_CONFIRMATION_LINE',
    NESTED: 'NESTED',
    TAX_LOOKUP: 'TAX_LOOKUP'
};
exports.childTypesForDocType = {
    INVOICE: [exports.lineTypes.NESTED, exports.lineTypes.INVOICE_LINE, exports.lineTypes.TAX_LOOKUP],
    CREDIT_NOTE: [exports.lineTypes.NESTED, exports.lineTypes.CREDIT_NOTE_LINE, exports.lineTypes.TAX_LOOKUP],
    MANUAL_INVOICE: [exports.lineTypes.NESTED, exports.lineTypes.MANUAL_INVOICE_LINE],
    MANUAL_CREDIT_NOTE: [exports.lineTypes.NESTED, exports.lineTypes.MANUAL_CREDIT_NOTE_LINE],
    STATEMENT: [exports.lineTypes.NESTED, exports.lineTypes.STATEMENT_INVOICE, exports.lineTypes.STATEMENT_CREDIT_NOTE, exports.lineTypes.STATEMENT_PAYMENT],
    DELIVERY_CONFIRMATION: [exports.lineTypes.NESTED, exports.lineTypes.DELIVERY_CONFIRMATION_LINE],
    DISMISS: [exports.lineTypes.NESTED],
    INVOICE_LINE: [exports.lineTypes.NESTED],
    CREDIT_NOTE_LINE: [exports.lineTypes.NESTED],
    STATEMENT_INVOICE: [exports.lineTypes.NESTED],
    STATEMENT_CREDIT_NOTE: [exports.lineTypes.NESTED],
    STATEMENT_PAYMENT: [exports.lineTypes.NESTED],
    MANUAL_INVOICE_LINE: [exports.lineTypes.NESTED],
    MANUAL_CREDIT_NOTE_LINE: [exports.lineTypes.NESTED],
    DELIVERY_CONFIRMATION_LINE: [exports.lineTypes.NESTED],
    NESTED: [exports.lineTypes.NESTED],
    TAX_LOOKUP: [exports.lineTypes.NESTED]
};
exports.lineTypeForDocType = {
    INVOICE: 'INVOICE_LINE',
    CREDIT_NOTE: 'CREDIT_NOTE_LINE',
    MANUAL_INVOICE: 'MANUAL_INVOICE_LINE',
    MANUAL_CREDIT_NOTE: 'MANUAL_CREDIT_NOTE_LINE',
    DELIVERY_CONFIRMATION: 'DELIVERY_CONFIRMATION_LINE'
};
// docType and lineType
exports.docTypeDisplayName = {
    INVOICE: 'INVOICE',
    CREDIT_NOTE: 'CREDIT NOTE',
    MANUAL_INVOICE: 'MANUAL INVOICE',
    MANUAL_CREDIT_NOTE: 'MANUAL CREDIT NOTE',
    DELIVERY_CONFIRMATION: 'DELIVERY CONFIRMATION',
    STATEMENT: 'STATEMENT',
    DISMISS: 'DISMISS',
    INVOICE_LINE: 'INVOICE LINE',
    CREDIT_NOTE_LINE: 'CREDIT NOTE LINE',
    STATEMENT_INVOICE: 'STATEMENT INVOICE',
    STATEMENT_CREDIT_NOTE: 'STATEMENT CREDIT NOTE',
    STATEMENT_PAYMENT: 'STATEMENT PAYMENT',
    MANUAL_INVOICE_LINE: 'MANUAL INVOICE LINE',
    MANUAL_CREDIT_NOTE_LINE: 'MANUAL CREDIT NOTE LINE',
    DELIVERY_CONFIRMATION_LINE: 'DELIVERY CONFIRMATION LINE',
    NESTED: 'NESTED',
    TAX_LOOKUP: 'TAX LOOKUP'
};
// reconciled status options for statement lines
exports.statementLineDocTypeNameMap = {
    STATEMENT_INVOICE: 'INVOICE',
    STATEMENT_CREDIT_NOTE: 'CREDIT NOTE',
    STATEMENT_PAYMENT: 'PAYMENT'
};
// export const level1DocTypes = [ // DEPRECATED - use docTypes
//   docTypes.INVOICE,
//   docTypes.CREDIT_NOTE,
//   docTypes.STATEMENT,
//   docTypes.DISMISS
// ];
// export const level2DocTypes = [ // DEPRECATED - use lineTypes
//   lineTypes.INVOICE_LINE,
//   lineTypes.CREDIT_NOTE_LINE,
//   lineTypes.STATEMENT_INVOICE,
//   lineTypes.STATEMENT_CREDIT_NOTE,
//   lineTypes.STATEMENT_PAYMENT,
//   lineTypes.NESTED
// ];
exports.dismissTypes = {
    ORDER: 'ORDER',
    QUOTE: 'QUOTE',
    PRO_FORMA: 'PRO_FORMA',
    TERMS_AND_CONDITIONS: 'TERMS_AND_CONDITIONS',
    INFORMATION: 'INFORMATION',
    MANUAL_INBOX: 'MANUAL_INBOX',
    DELIVERY_NOTE: 'DELIVERY_NOTE',
    REPORT: 'REPORT',
    DIRECT_DEBIT_NOTIFICATION: 'DIRECT_DEBIT_NOTIFICATION',
    DELIVERY_NOTIFICATION: 'DELIVERY_NOTIFICATION',
    PAYMENT_REMINDER: 'PAYMENT_REMINDER',
    PAYMENT_CONFIRMATION: 'PAYMENT_CONFIRMATION',
    RECEIPT: 'RECEIPT',
    REPLY_EMAIL: 'REPLY_EMAIL',
    BLANK: 'BLANK'
};
exports.dismissTypeDisplayNames = {
    ORDER: 'ORDER',
    QUOTE: 'QUOTE',
    PRO_FORMA: 'PRO FORMA',
    TERMS_AND_CONDITIONS: 'TERMS AND CONDITIONS',
    INFORMATION: 'INFORMATION',
    MANUAL_INBOX: 'MANUAL INBOX',
    DELIVERY_NOTE: 'DELIVERY NOTE',
    REPORT: 'REPORT',
    DIRECT_DEBIT_NOTIFICATION: 'DIRECT DEBIT NOTIFICATION',
    DELIVERY_NOTIFICATION: 'DELIVERY NOTIFICATION',
    PAYMENT_REMINDER: 'PAYMENT REMINDER',
    PAYMENT_CONFIRMATION: 'PAYMENT CONFIRMATION',
    RECEIPT: 'RECEIPT',
    BLANK: 'BLANK'
};
