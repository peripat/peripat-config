export declare const inboxItemResults: {
    PROCESSED: string;
    INCOMPLETE: string;
    NOTIFICATION: string;
    ON_HOLD: string;
};
export declare const postmarkResults: {
    NEW: string;
    DONE: string;
    NOT_RUN: string;
    ERROR: string;
};
export declare const inboxItemTypes: {
    EMAIL: string;
    FILE: string;
};
