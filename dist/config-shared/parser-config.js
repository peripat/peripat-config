"use strict";
exports.__esModule = true;
exports.defaultParserEngineVersions = exports.parserEngineVersions = exports.engineVersions = exports.parserEngines = void 0;
exports.parserEngines = {
    EMAIL: 'EMAIL',
    PDF: 'PDF',
    IMAGE: 'IMAGE',
    SPREADSHEET: 'SPREADSHEET',
    DOCUMENT: 'DOCUMENT',
    TEXT: 'TEXT',
    HTML: 'HTML',
    JSON: 'JSON',
    XML: 'XML',
    CSV: 'CSV'
};
exports.engineVersions = {
    v1: 'v1',
    'R270_v1': 'R270_v1',
    RAW: 'RAW'
};
exports.parserEngineVersions = {
    EMAIL: [exports.engineVersions.v1],
    PDF: [exports.engineVersions.v1, exports.engineVersions.R270_v1],
    IMAGE: [exports.engineVersions.v1],
    SPREADSHEET: [exports.engineVersions.v1],
    DOCUMENT: [exports.engineVersions.v1],
    TEXT: [exports.engineVersions.v1, exports.engineVersions.RAW],
    HTML: [exports.engineVersions.v1, exports.engineVersions.RAW],
    JSON: [exports.engineVersions.v1, exports.engineVersions.RAW],
    XML: [exports.engineVersions.v1, exports.engineVersions.RAW],
    CSV: [exports.engineVersions.v1, exports.engineVersions.RAW]
};
exports.defaultParserEngineVersions = {
    EMAIL: [
        {
            parserEngine: exports.parserEngines.EMAIL,
            engineVersion: exports.engineVersions.v1
        }
    ],
    PDF: [
        {
            parserEngine: exports.parserEngines.PDF,
            engineVersion: exports.engineVersions.v1
        }
    ],
    IMAGE: [
        {
            parserEngine: exports.parserEngines.IMAGE,
            engineVersion: exports.engineVersions.v1
        }
    ],
    SPREADSHEET: [
        {
            parserEngine: exports.parserEngines.SPREADSHEET,
            engineVersion: exports.engineVersions.v1
        }
    ],
    DOCUMENT: [
        {
            parserEngine: exports.parserEngines.DOCUMENT,
            engineVersion: exports.engineVersions.v1
        }
    ],
    TEXT: [
        {
            parserEngine: exports.parserEngines.TEXT,
            engineVersion: exports.engineVersions.v1
        }
    ],
    HTML: [
        {
            parserEngine: exports.parserEngines.HTML,
            engineVersion: exports.engineVersions.v1
        }
    ],
    JSON: [
        {
            parserEngine: exports.parserEngines.JSON,
            engineVersion: exports.engineVersions.RAW
        }
    ],
    XML: [
        {
            parserEngine: exports.parserEngines.XML,
            engineVersion: exports.engineVersions.RAW
        }
    ],
    CSV: [
        {
            parserEngine: exports.parserEngines.CSV,
            engineVersion: exports.engineVersions.RAW
        }
    ],
    COMPRESSED: [],
    EML: [],
    UNKNOWN_FILE: []
};
