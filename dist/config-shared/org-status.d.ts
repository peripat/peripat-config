export declare const orgStatuses: {
    ACTIVE: string;
    ARCHIVED: string;
    DRAFT: string;
    CLOSED: string;
};
export declare const inboxStatuses: {
    USER_HOLD: string;
    ADMIN_HOLD: string;
    ACTIVE: string;
    ARCHIVED: string;
    DRAFT: string;
    CLOSED: string;
};
