export declare const roles: {
    SYSTEM_ADMIN_GOD: string;
    SYSTEM_ADMIN_ORG: string;
    SYSTEM_ADMIN_PATTERN: string;
    INBOX_ACCESS_VIEW: string;
    INBOX_ACCESS_SEND_EMAIL: string;
    INBOX_ACCESS_RUN_ACTIONS: string;
    DOC_ACCESS_VIEW: string;
    DOC_ACCESS_RUN_ACTIONS: string;
    DOC_ACCESS_DELETE: string;
    DOC_ACCESS_APPROVE: string;
    ORG_ADMIN_VIEW: string;
    ORG_ADMIN_EDIT: string;
    USER_ADMIN_EDIT: string;
    BASIC_ACCESS: string;
};
