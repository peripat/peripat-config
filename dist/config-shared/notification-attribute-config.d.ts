export declare const notificationSeverities: {
    MESSAGE: string;
    WARNING: string;
    ACTION_REQUIRED: string;
    ERROR: string;
};
export declare const notificationAssignedTos: {
    ORG: string;
    ADMIN: string;
};
