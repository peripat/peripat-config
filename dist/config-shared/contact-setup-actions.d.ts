export declare const contactSetupActions: {
    ADD_CONTACT_TO_ORG: string;
    ADD_SUPPLIER_ACCOUNTS: string;
    ADD_NEW_GLOBAL_CONTACT: string;
    ACTIVATE_DRAFT_GLOBAL_CONTACT: string;
};
