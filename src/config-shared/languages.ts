export const languages = {
  en: 'en',
  es: 'es',
  fr: 'fr',
  it: 'it',
  de: 'de',
  pt: 'pt',
};

export const languageProperties = [
  { language: languages.en, languageName: 'English' },
  { language: languages.fr, languageName: 'French' },
  { language: languages.de, languageName: 'German' },
  { language: languages.it, languageName: 'Italian' },
  { language: languages.pt, languageName: 'Portuguese' },
  { language: languages.es, languageName: 'Spanish' }
];
