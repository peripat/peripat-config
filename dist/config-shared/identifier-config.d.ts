export declare const identifierResults: {
    PASS: string;
    FAIL: string;
    INVALID: string;
};
