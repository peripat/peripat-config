export declare const notificationActions: {
    ADD_NOTE: string;
    PROBLEM_FOUND: string;
    RESOLVE_FOR_USER: string;
    RESOLVE_FOR_ADMIN: string;
    SELECT_CONTACT_FOR_INBOX_ITEM: string;
    RERUN_INBOX_ITEM: string;
    RERUN_SUB_ITEM: string;
    RERUN_DOC: string;
    FORCE_STATEMENT_TO_RUN: string;
    ADD_TO_INBOX_BLOCK_LIST: string;
    SKIP_INBOX_ITEM: string;
    ALLOW_INBOX_ITEM_PROCESSING: string;
    SELECT_CONTACT_FOR_SUB_ITEM: string;
    SKIP_SUB_ITEM: string;
    ASSIGN_TO_USER: string;
    SUSPECT_PHISHING: string;
    DELETE_SUB_ITEM_AND_RERUN: string;
    DELETE_SUB_ITEM_DOCS_AND_RERUN: string;
    DELETE_DOC_AND_RERUN: string;
    SKIP_DOC: string;
    SELECT_SUPPLIER_ACCOUNT: string;
    SET_CURRENCY: string;
    SET_PAYMENT_METHOD: string;
    REPLACE_EXISTING_DOC: string;
    RERUN_ACCOUNTING_EXPORT_FOR_DOC: string;
    SKIP_ACCOUNTING_EXPORT: string;
    MUTE_EMAIL: string;
    CLEAR_NOTIFICATION: string;
    SETUP_SUPPLIER_ACCOUNT_FROM_DOC: string;
    CREATE_MANUAL_INVOICE: string;
};
export declare const notificationActionIsBulk: {
    ADD_NOTE: boolean;
    PROBLEM_FOUND: boolean;
    RESOLVE_FOR_USER: boolean;
    RESOLVE_FOR_ADMIN: boolean;
    SELECT_CONTACT_FOR_INBOX_ITEM: boolean;
    RERUN_INBOX_ITEM: boolean;
    RERUN_SUB_ITEM: boolean;
    RERUN_DOC: boolean;
    FORCE_STATEMENT_TO_RUN: boolean;
    ADD_TO_INBOX_BLOCK_LIST: boolean;
    SKIP_INBOX_ITEM: boolean;
    ALLOW_INBOX_ITEM_PROCESSING: boolean;
    SELECT_CONTACT_FOR_SUB_ITEM: boolean;
    SKIP_SUB_ITEM: boolean;
    ASSIGN_TO_USER: boolean;
    SUSPECT_PHISHING: boolean;
    DELETE_SUB_ITEM_AND_RERUN: boolean;
    DELETE_SUB_ITEM_DOCS_AND_RERUN: boolean;
    DELETE_DOC_AND_RERUN: boolean;
    SKIP_DOC: boolean;
    SELECT_SUPPLIER_ACCOUNT: boolean;
    SET_CURRENCY: boolean;
    SET_PAYMENT_METHOD: boolean;
    REPLACE_EXISTING_DOC: boolean;
    RERUN_ACCOUNTING_EXPORT_FOR_DOC: boolean;
    SKIP_ACCOUNTING_EXPORT: boolean;
    MUTE_EMAIL: boolean;
    CLEAR_NOTIFICATION: boolean;
    SETUP_SUPPLIER_ACCOUNT_FROM_DOC: boolean;
    CREATE_MANUAL_INVOICE: boolean;
};
