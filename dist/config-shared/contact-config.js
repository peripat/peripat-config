"use strict";
exports.__esModule = true;
exports.capturedItemTypes = exports.parentContactTypes = void 0;
exports.parentContactTypes = {
    ACCOUNTING: 'ACCOUNTING',
    COMPANY_GROUP: 'COMPANY_GROUP',
    PAYMENTS: 'PAYMENTS',
    DELIVERIES: 'DELIVERIES',
    FINANCE: 'FINANCE',
    ASSET_MANAGEMENT: 'ASSET_MANAGEMENT'
};
exports.capturedItemTypes = {
    PATTERN: 'PATTERN',
    FIELD: 'FIELD'
};
