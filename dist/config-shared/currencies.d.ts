export declare const currencies: {
    AUD: string;
    CAD: string;
    CHF: string;
    EUR: string;
    GBP: string;
    JPY: string;
    NOK: string;
    NZD: string;
    SEK: string;
    USD: string;
};
export declare const currencyProperties: {
    currency: string;
    currencySymbol: string;
    currencyName: string;
}[];
