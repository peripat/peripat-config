export declare const languages: {
    en: string;
    es: string;
    fr: string;
    it: string;
    de: string;
    pt: string;
};
export declare const languageProperties: {
    language: string;
    languageName: string;
}[];
