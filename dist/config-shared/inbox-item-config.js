"use strict";
// also update inbox-item-name-map.js
exports.__esModule = true;
exports.inboxItemTypes = exports.postmarkResults = exports.inboxItemResults = void 0;
exports.inboxItemResults = {
    PROCESSED: 'PROCESSED',
    INCOMPLETE: 'INCOMPLETE',
    NOTIFICATION: 'NOTIFICATION',
    ON_HOLD: 'ON_HOLD'
};
exports.postmarkResults = {
    NEW: 'NEW',
    DONE: 'DONE',
    NOT_RUN: 'NOT_RUN',
    ERROR: 'ERROR' // this is set but should only be temp state. Will need to be switched to NOT_RUN manually if it is not going to be run
};
exports.inboxItemTypes = {
    EMAIL: 'EMAIL',
    FILE: 'FILE'
};
