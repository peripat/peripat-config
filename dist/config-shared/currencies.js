"use strict";
// https://en.wikipedia.org/wiki/ISO_4217
exports.__esModule = true;
exports.currencyProperties = exports.currencies = void 0;
exports.currencies = {
    AUD: 'AUD',
    CAD: 'CAD',
    CHF: 'CHF',
    EUR: 'EUR',
    GBP: 'GBP',
    JPY: 'JPY',
    NOK: 'NOK',
    NZD: 'NZD',
    SEK: 'SEK',
    USD: 'USD'
};
exports.currencyProperties = [
    { currency: exports.currencies.AUD, currencySymbol: '$', currencyName: 'Australian Dollar' },
    { currency: exports.currencies.CAD, currencySymbol: '$', currencyName: 'Canadian Dollar' },
    { currency: exports.currencies.CHF, currencySymbol: 'Fr', currencyName: 'Swiss Franc' },
    { currency: exports.currencies.EUR, currencySymbol: '€', currencyName: 'Euro' },
    { currency: exports.currencies.GBP, currencySymbol: '£', currencyName: 'British Pound Sterling' },
    { currency: exports.currencies.JPY, currencySymbol: '¥', currencyName: 'Japanese Yen' },
    { currency: exports.currencies.NOK, currencySymbol: 'kr', currencyName: 'Norwegian Krone' },
    { currency: exports.currencies.NZD, currencySymbol: '$', currencyName: 'New Zealand Dollar' },
    { currency: exports.currencies.SEK, currencySymbol: 'kr', currencyName: 'Swedish Krona' },
    { currency: exports.currencies.USD, currencySymbol: '$', currencyName: 'United States Dollar' }
];
