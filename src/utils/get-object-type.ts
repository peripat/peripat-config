import { objectTypes, objectTypeIdPrefixes } from '../config-shared';

/**
 * Gets the object type from either the id given or the object._id. It is best to provide
 * the object which has an _id to work out what the object type is. The other methods are if
 * they have to be used
 * @param {(string|object)} id - this can be a string or an object with a ._id field
 * @param {object} object - OPTIONAL. This can be provided to help determin if an id.
 * @returns {string}
 */
export function getObjectTypeFromId(id, object) {
  if (!id) return;

  let hasOrgId;
  if (typeof id === 'string' && object) {
    hasOrgId = !!object.orgId; // stores if the object has a orgId
  }

  if (typeof id === 'object') {
    hasOrgId = !!id.orgId; // stores if the object has a orgId
    id = id._id;
  }
  if (typeof id !== 'string') return;
  let idPrefix = id.substring(0, 3);
  let objectType = Object.keys(objectTypeIdPrefixes).find(
    key => objectTypeIdPrefixes[key] === idPrefix
  );

  // if it is a contact and we had the object earlier, determine whether it is a global or org contact
  if (objectType === objectTypes.ORG_CONTACT && hasOrgId === false) {
    objectType = objectTypes.GLOBAL_CONTACT;
  } else if (objectType === objectTypes.GLOBAL_CONTACT && hasOrgId === true) {
    objectType = objectTypes.ORG_CONTACT;
  }
  return objectType;
}
