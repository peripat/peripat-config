"use strict";
exports.__esModule = true;
exports.contactSetupActions = void 0;
exports.contactSetupActions = {
    ADD_CONTACT_TO_ORG: 'ADD_CONTACT_TO_ORG',
    ADD_SUPPLIER_ACCOUNTS: 'ADD_SUPPLIER_ACCOUNTS',
    ADD_NEW_GLOBAL_CONTACT: 'ADD_NEW_GLOBAL_CONTACT',
    ACTIVATE_DRAFT_GLOBAL_CONTACT: 'ACTIVATE_DRAFT_GLOBAL_CONTACT'
};
