import { subItemTypes, extensionsForSubItemTypes } from '../config-shared';

/**
 * Gets the sub item type
 * @param {string} extension - The file extension
 * @returns {string}
 */
 export function getSubItemTypeFromFileExtension(extension = '') {
  let ext = extension.toLowerCase();
  let subItemType = Object.keys(extensionsForSubItemTypes).find(key =>
    extensionsForSubItemTypes[key].includes(ext)
  );
  return subItemType || subItemTypes.UNKNOWN_FILE;
}
