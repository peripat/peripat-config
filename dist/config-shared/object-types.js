"use strict";
exports.__esModule = true;
exports.objectTypeParentType = exports.objectTypeChildType = exports.objectTypeArrayFieldName = exports.objectTypeReferenceFieldName = exports.objectTypeReferenceIdFieldName = exports.objectTypeNameMap = exports.objectTypeIdPrefixes = exports.objectTypes = void 0;
exports.objectTypes = {
    INBOX_ITEM: 'INBOX_ITEM',
    SUB_ITEM: 'SUB_ITEM',
    DOC: 'DOC',
    LINE: 'LINE',
    NOTIFICATION: 'NOTIFICATION',
    SUPPLIER_ACCOUNT: 'SUPPLIER_ACCOUNT',
    PATTERN: 'PATTERN',
    GLOBAL_CONTACT: 'GLOBAL_CONTACT',
    STORED_FILE: 'STORED_FILE',
    ORG: 'ORG',
    INBOX: 'INBOX',
    COMPANY: 'COMPANY',
    SITE: 'SITE',
    ORG_CONTACT: 'ORG_CONTACT',
    SENT_EMAIL: 'SENT_EMAIL',
    USER: 'USER',
    EXTERNAL_CONTACT: 'EXTERNAL_CONTACT',
    APP_KEY_VALUE: 'APP_KEY_VALUE'
};
exports.objectTypeIdPrefixes = {
    INBOX_ITEM: 'INI',
    SUB_ITEM: 'SUB',
    DOC: 'DOC',
    LINE: 'LIN',
    NOTIFICATION: 'NOT',
    SUPPLIER_ACCOUNT: 'SAC',
    PATTERN: 'PAT',
    GLOBAL_CONTACT: 'OCT',
    STORED_FILE: 'FIL',
    ORG: 'ORG',
    INBOX: 'INB',
    COMPANY: 'COM',
    SITE: 'STE',
    ORG_CONTACT: 'OCT',
    SENT_EMAIL: 'SNT',
    USER: 'USR',
    EXTERNAL_CONTACT: 'ECT',
    APP_KEY_VALUE: 'AKV'
};
exports.objectTypeNameMap = {
    INBOX_ITEM: 'Inbox Item',
    SUB_ITEM: 'Sub Item',
    DOC: 'Doc',
    LINE: 'Line',
    NOTIFICATION: 'Notification',
    SUPPLIER_ACCOUNT: 'Supplier Account',
    PATTERN: 'Pattern',
    GLOBAL_CONTACT: 'Global Contact',
    STORED_FILE: 'File',
    ORG: 'Organisation',
    INBOX: 'Inbox',
    COMPANY: 'Company',
    SITE: 'Site',
    ORG_CONTACT: 'Contact',
    SENT_EMAIL: 'Sent Email',
    USER: 'User',
    EXTERNAL_CONTACT: 'External Contact',
    APP_KEY_VALUE: 'App Key Value'
};
exports.objectTypeReferenceIdFieldName = {
    INBOX_ITEM: 'inboxItemId',
    SUB_ITEM: 'subItemId',
    DOC: 'docId',
    LINE: 'lineId',
    NOTIFICATION: 'notificationId',
    SUPPLIER_ACCOUNT: 'supplierAccountId',
    PATTERN: 'patternId',
    GLOBAL_CONTACT: 'contactId',
    STORED_FILE: 'fileId',
    ORG: 'orgId',
    INBOX: 'inboxId',
    COMPANY: 'companyId',
    SITE: 'siteId',
    ORG_CONTACT: 'contactId',
    SENT_EMAIL: 'sentEmailId',
    USER: 'userId',
    EXTERNAL_CONTACT: 'externalContactId'
};
exports.objectTypeReferenceFieldName = {
    INBOX_ITEM: 'inboxItem',
    SUB_ITEM: 'subItem',
    DOC: 'doc',
    LINE: 'line',
    NOTIFICATION: 'notification',
    SUPPLIER_ACCOUNT: 'supplierAccount',
    PATTERN: 'pattern',
    GLOBAL_CONTACT: 'contact',
    STORED_FILE: 'file',
    ORG: 'org',
    INBOX: 'inbox',
    COMPANY: 'company',
    SITE: 'site',
    ORG_CONTACT: 'contact',
    SENT_EMAIL: 'sentEmail',
    USER: 'user',
    EXTERNAL_CONTACT: 'externalContact'
};
exports.objectTypeArrayFieldName = {
    INBOX_ITEM: 'inboxItems',
    SUB_ITEM: 'subItems',
    DOC: 'docs',
    LINE: 'lines',
    NOTIFICATION: 'notifications',
    SUPPLIER_ACCOUNT: 'supplierAccounts',
    PATTERN: 'patterns',
    GLOBAL_CONTACT: 'contacts',
    STORED_FILE: 'files',
    ORG: 'orgs',
    INBOX: 'inboxes',
    COMPANY: 'companies',
    SITE: 'sites',
    ORG_CONTACT: 'contacts',
    SENT_EMAIL: 'sentEmails',
    USER: 'users',
    EXTERNAL_CONTACT: 'externalContacts'
};
exports.objectTypeChildType = {
    INBOX_ITEM: exports.objectTypes.SUB_ITEM,
    SUB_ITEM: exports.objectTypes.DOC,
    DOC: exports.objectTypes.LINE
};
exports.objectTypeParentType = {
    // INBOX_ITEM: '',
    SUB_ITEM: exports.objectTypes.INBOX_ITEM,
    DOC: exports.objectTypes.SUB_ITEM,
    LINE: exports.objectTypes.DOC
};
