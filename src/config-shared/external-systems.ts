export const accountingSystems = {
  XERO: 'XERO',
  SAGE: 'SAGE',
  QUICKBOOKS: 'QUICKBOOKS',
  FREEAGENT: 'FREEAGENT',
  NETSUITE: 'NETSUITE',
  KASHFLOW: 'KASHFLOW',
  TWINFIELD: 'TWINFIELD',
  AQUILLA: 'AQUILLA',
  // if anything is added, also update in externalSystemNameMap external-system-name-map.js
  OTHER_ACCOUNTING: 'OTHER_ACCOUNTING'
};

export const orderingSystems = {
  MARKETMAN: 'MARKETMAN',
  TEVALIS: 'TEVALIS',
  FNB_SHOP: 'FNB_SHOP',
  PRONETT: 'PRONETT',
  OTHER_ORDERING: 'OTHER_ORDERING'
};

export const externalSystems = {
  EMAIL: 'EMAIL',
  ...accountingSystems,
  ...orderingSystems
};

export const externalSystemSettingsNames = {
  XERO: 'xeroSettings',
  SAGE: 'sageSettings',
  QUICKBOOKS: 'quickbooksSettings',
  FREEAGENT: 'freeagentSettings',
  NETSUITE: 'netsuiteSettings',
  KASHFLOW: 'kashflowSettings',
  TWINFIELD: 'twinfieldSettings',
  AQUILLA: 'aquillaSettings',
  OTHER_ACCOUNTING: 'otherAccountingSettings',
  MARKETMAN: 'marketmanSettings',
  TEVALIS: 'tevalisSettings',
  FNB_SHOP: 'fnbShopSettings',
  PRONETT: 'pronettSettings'
};
