export declare const parserEngines: {
    EMAIL: string;
    PDF: string;
    IMAGE: string;
    SPREADSHEET: string;
    DOCUMENT: string;
    TEXT: string;
    HTML: string;
    JSON: string;
    XML: string;
    CSV: string;
};
export declare const engineVersions: {
    v1: string;
    R270_v1: string;
    RAW: string;
};
export declare const parserEngineVersions: {
    EMAIL: string[];
    PDF: string[];
    IMAGE: string[];
    SPREADSHEET: string[];
    DOCUMENT: string[];
    TEXT: string[];
    HTML: string[];
    JSON: string[];
    XML: string[];
    CSV: string[];
};
export declare const defaultParserEngineVersions: {
    EMAIL: {
        parserEngine: string;
        engineVersion: string;
    }[];
    PDF: {
        parserEngine: string;
        engineVersion: string;
    }[];
    IMAGE: {
        parserEngine: string;
        engineVersion: string;
    }[];
    SPREADSHEET: {
        parserEngine: string;
        engineVersion: string;
    }[];
    DOCUMENT: {
        parserEngine: string;
        engineVersion: string;
    }[];
    TEXT: {
        parserEngine: string;
        engineVersion: string;
    }[];
    HTML: {
        parserEngine: string;
        engineVersion: string;
    }[];
    JSON: {
        parserEngine: string;
        engineVersion: string;
    }[];
    XML: {
        parserEngine: string;
        engineVersion: string;
    }[];
    CSV: {
        parserEngine: string;
        engineVersion: string;
    }[];
    COMPRESSED: any[];
    EML: any[];
    UNKNOWN_FILE: any[];
};
