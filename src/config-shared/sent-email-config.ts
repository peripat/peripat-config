// see also postmark-config for postmark specific config

export const sentEmailStatuses = {
  AWAITING_DELIVERY: 'AWAITING_DELIVERY', // when we send an email and we are waiting for the response from postmark via webhook to get the email content
  DELIVERED: 'DELIVERED', // Email has been delivered (or could be blocked) and we have stored it
  UNLINKED: 'UNLINKED' // An email that has come in via the delivery/bounce hook but we couldn't find a corresponding email to link it to (might not have been stored yet)
};
