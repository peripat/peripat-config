export declare const docActions: {
    APPROVE: string;
    REJECT: string;
    REJECT_DELETE: string;
    DELETE: string;
    RESET_APPROVALS: string;
};
export declare const docActionIsBulk: {
    APPROVE: boolean;
    REJECT: boolean;
    REJECT_DELETE: boolean;
    DELETE: boolean;
    RESET_APPROVALS: boolean;
};
export declare const manualInvoiceActions: {
    CREATE_MANUAL_INVOICE: string;
};
