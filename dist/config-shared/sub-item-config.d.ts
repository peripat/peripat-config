export declare const subItemTypes: {
    EMAIL: string;
    PDF: string;
    IMAGE: string;
    SPREADSHEET: string;
    DOCUMENT: string;
    TEXT: string;
    HTML: string;
    JSON: string;
    XML: string;
    CSV: string;
    COMPRESSED: string;
    EML: string;
    UNKNOWN_FILE: string;
};
export declare const subItemResults: {
    PROCESSED: string;
    INCOMPLETE: string;
    NOTIFICATION: string;
};
export declare const extensionsForSubItemTypes: {
    PDF: string[];
    IMAGE: string[];
    SPREADSHEET: string[];
    DOCUMENT: string[];
    TEXT: string[];
    HTML: string[];
    JSON: string[];
    XML: string[];
    CSV: string[];
    COMPRESSED: string[];
    EML: string[];
};
export declare const sendAttachmentExtensions: any[];
